import { Controller, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern, ClientProxy } from '@nestjs/microservices';
import { UserDto } from './dto/user.dto';
import { LoginResponse } from './dto/login-response.dto';
const SERVICE_NAME = 'auth';
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject('SERVICES') private readonly client: ClientProxy,
  ) {}

  @MessagePattern({ service: SERVICE_NAME, cmd: 'login' })
  async login(data: UserDto): Promise<LoginResponse> {
    const response = await this.appService.login(data.username, data.password);
    this.client.emit(
      { service: SERVICE_NAME, cmd: 'logged' },
      { request: data, response },
    );
    return response;
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'signUp' })
  async signUp(data: UserDto): Promise<any> {
    const response = await this.appService.signUp(data.username, data.password);
    this.client.emit(
      { service: SERVICE_NAME, cmd: 'signedUp' },
      { request: data, response },
    );
    return response;
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'userByToken' })
  async userByToken(
    data: any,
  ): Promise<null | { [key: string]: any } | string> {
    const response = await this.appService.userByToken(data.token);
    return response;
  }
}
