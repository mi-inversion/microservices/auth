import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
const UserSchema = new mongoose.Schema({
  username: { type: String, unique: true },
  password: String,
});

// hash the password
UserSchema.methods.generateHash = password => {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
};

// checking if password is valid
UserSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

export { UserSchema };
