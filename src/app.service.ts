import { Injectable } from '@nestjs/common';
import { UserRepository } from './repositories/users.repository';
import * as jwt from 'jsonwebtoken';
import { ConfigService } from '@nestjs/config';
import { RpcException } from '@nestjs/microservices';

const JWT_SECRET_ENV = 'JWT_SECRET';
const JWT_EXPIRATION = 432000000;

@Injectable()
export class AppService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly configService: ConfigService,
  ) {}

  async login(username: string, password: string) {
    const isPasswordValid = await this.userRepository.validatePassword({
      username,
      password,
    });

    if (isPasswordValid) {
      const user = await this.userRepository.userModel.findOne({ username });
      const secret = this.configService.get(JWT_SECRET_ENV);
      const token = jwt.sign({ user }, secret, {
        expiresIn: JWT_EXPIRATION,
      });
      return { username, token };
    } else {
      throw new RpcException('INVALID_PASSWORD');
    }
  }

  async signUp(username: string, password: string) {
    console.log({ username, password });
    return await this.userRepository.create({ username, password });
  }

  async userByToken(token: string) {
    const secret = this.configService.get(JWT_SECRET_ENV);
    const isValid = jwt.verify(token, secret);
    if (isValid) {
      return jwt.decode(token);
    } else {
      throw new RpcException('INVALID_TOKEN');
    }
  }
}
