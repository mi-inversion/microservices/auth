import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDto } from '../dto/user.dto';
import { RpcException } from '@nestjs/microservices';

@Injectable()
export class UserRepository {
  constructor(@InjectModel('User') public readonly userModel: Model<any>) {}

  async create(user: UserDto) {
    const newUser = await this.userModel.create(user);
    newUser.password = newUser.generateHash(user.password);
    const userWithPassword = await newUser.save();
    userWithPassword.password = undefined;
    return userWithPassword;
  }

  async validatePassword(userDto: UserDto) {
    const user = await this.userModel.findOne({ username: userDto.username });
    if (!user) {
      throw new RpcException('NO USER FOUND');
    } else {
      return user.validPassword(userDto.password);
    }
  }
}
